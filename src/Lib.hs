module Lib where

import           Lib.Prelude

someFunc :: IO ()
someFunc = do
  let tohundred = [1 .. 100 :: Int]
  print $ tailSafe $ reverse tohundred
  print $ maximum $ concat $ findAllDiffs [1 .. 100]
  print $ maximum $ concat $ findAllDiffs [100, 99 .. 1]

findAllDiffs :: [Int] -> [[Int]]
findAllDiffs [] = [[-1]]
findAllDiffs xs =
  let rest = reverse $ tailSafe $ reverse xs
   in (findDiff $ reverse xs) : (findAllDiffs rest)

findDiff :: [Int] -> [Int]
findDiff []     = [-1]
findDiff [_]    = [-1]
findDiff (x:xs) = map (\y -> x - y) xs
